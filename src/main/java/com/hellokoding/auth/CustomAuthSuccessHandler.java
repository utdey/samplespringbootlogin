/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.auth;

import com.hellokoding.auth.model.User;
import com.hellokoding.auth.repository.UserRepository;
import com.hellokoding.auth.service.UtilityService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

/**
 *
 * @author subhajgh
 */
@Component
public class CustomAuthSuccessHandler extends
        SavedRequestAwareAuthenticationSuccessHandler {

    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Autowired
    UserRepository userRepo;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest hsr, HttpServletResponse hsr1, Authentication a) throws IOException, ServletException {
        //super.onAuthenticationSuccess(hsr, hsr1, a);
        System.out.println("success authentication");

        User user = userRepo.findByUsername(a.getName());
        if (user.isTwoFactorEnabled()) {
            Cookie c = new Cookie("http-secure-id", "pending");
            c.setHttpOnly(true);
            hsr1.addCookie(c);
            redirectStrategy.sendRedirect(hsr, hsr1, "/otplogin");
        } else {
            if (UtilityService.checkCookieExists(hsr, "http-secure-id", "pending")) {
                UtilityService.modifyCookie(hsr, hsr1, "http-secure-id", "verified");
            } else {
                Cookie c = new Cookie("http-secure-id", "verified");
                c.setHttpOnly(true);
                hsr1.addCookie(c);
            }
            redirectStrategy.sendRedirect(hsr, hsr1, "/welcome");
        }

    }

}
