package com.hellokoding.auth.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hellokoding.auth.model.User;
import com.hellokoding.auth.model.UserVerifyModel;
import com.hellokoding.auth.service.SecurityService;
import com.hellokoding.auth.service.SecurityServiceImpl;
import com.hellokoding.auth.service.UserService;
import com.hellokoding.auth.service.UtilityService;
import com.hellokoding.auth.validator.UserValidator;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Controller
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @Value("${twofactor.clientid}")
    String clientid;
    @Value("${twofactor.client.secret}")
    String clientSecret;
    @Value("${twofactor.verify.url}")
    String clientUrl;

    Map<String, String> userMap = new HashMap<>();

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model, String otpserver) {
        model.addAttribute("userForm", new User());
        if (otpserver != null) {
            model.addAttribute("error", "Otp Server failed to register ... Please contact administrator");
        }
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) throws IOException {
        userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userService.save(userForm);
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout,  HttpServletRequest request, HttpServletResponse resp) {
        if (error != null) {
            model.addAttribute("error", "Your username and password is invalid.");
        }

        if (logout != null) {
            model.addAttribute("message", "You have been logged out successfully.");
        }

         UtilityService.clearCookie(request, resp, "http-secure-id");

        return "login";
    }

    @RequestMapping(value = "/otplogin", method = RequestMethod.GET)
    public String otplogin(Model model, String otperror, String logout, HttpServletRequest request) {
        if (otperror != null) {
            //eraseCookie(request, resp, "http-secure-id");
            model.addAttribute("message", "Invalid OTP provided");
        }
        return "otplogin";
    }

    private void modifyCookie(HttpServletRequest req, HttpServletResponse resp, String cookieName, String val) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieName)) {
                    cookie.setValue(val);
                    cookie.setHttpOnly(true);
                    resp.addCookie(cookie);
                }
            }
        }
    }

    private void eraseCookie(HttpServletRequest req, HttpServletResponse resp, String cookieName) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieName)) {
                    cookie.setValue("");
                    cookie.setMaxAge(0);
                    cookie.setPath("/");
                    cookie.setHttpOnly(true);
                    resp.addCookie(cookie);
                }
            }
        }
    }

    @RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model, HttpServletRequest request) {
        Authentication acc = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(acc.getName());
        model.addAttribute("twoFactorEnabled",user.isTwoFactorEnabled());
        return "welcome";
    }
    
    @RequestMapping(value = "/twofactorlogin", method = RequestMethod.POST)
    public String twofactorlogin(@ModelAttribute UserVerifyModel userModel, HttpServletRequest request, HttpServletResponse resp) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        try {
            Authentication acc = SecurityContextHolder.getContext().getAuthentication();
            logger.info(acc.getName());

           //  userModel.setOrgId("1");
            userModel.setUsername(acc.getName());
            System.out.println(clientUrl);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            String base64String = Base64.getEncoder().encodeToString((clientid + ":" + clientSecret).getBytes());
            headers.add("Authorization", "Basic " + base64String);
            HttpEntity<UserVerifyModel> httpEntity = new HttpEntity(userModel, headers);
            logger.info("hitting the two factor verification api.....");
            ResponseEntity<String> response = restTemplate.exchange(clientUrl + "/verify", HttpMethod.POST, httpEntity, String.class);
            if (response.getStatusCode() == HttpStatus.OK && response.getBody().contains("verified")) {
                logger.info("verified success... redireted to welcome page");
                UtilityService.modifyCookie(request, resp, "http-secure-id", "verified");
                return "redirect:/welcome";
            }
        } catch (RestClientException e) {
            logger.info("code not verified... redireted to logout page " + e.getMessage());
        }
        return "redirect:/otplogin?otperror";
    }
}
