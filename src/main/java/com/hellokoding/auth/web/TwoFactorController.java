/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.auth.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hellokoding.auth.model.User;
import com.hellokoding.auth.model.UserVerifyModel;
import com.hellokoding.auth.service.SecurityService;
import com.hellokoding.auth.service.UserService;
import com.hellokoding.auth.service.UtilityService;
import com.hellokoding.auth.validator.UserValidator;
import java.io.IOException;
import java.util.Base64;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author subhajgh
 */
@RestController
public class TwoFactorController {

    private static final Logger logger = LoggerFactory.getLogger(TwoFactorController.class);
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserValidator userValidator;

    @Value("${twofactor.clientid}")
    String clientid;
    @Value("${twofactor.client.secret}")
    String clientSecret;
    @Value("${twofactor.verify.url}")
    String clientUrl;

    @RequestMapping(value = "/twofactor/enable", method = RequestMethod.GET)
    public Object twofactorlogin(HttpServletRequest request, HttpServletResponse resp) throws IOException {

        try {
            Authentication acc = SecurityContextHolder.getContext().getAuthentication();
            logger.info(acc.getName());

            User user = new User();

            user.setUsername(acc.getName());
            if (registerForTwoFactor(user)) {
                return new ResponseEntity("{\"orgId\":" + user.getOrgid()+",\"verificationCode\":\"" + user.getVericationCode()+"\"}", HttpStatus.OK);
            }

        } catch (RestClientException e) {
            logger.info("code not verified... redireted to logout page " + e.getMessage());
        }
        return new ResponseEntity("error", HttpStatus.SERVICE_UNAVAILABLE);
    }

    @RequestMapping(value = "/twofactor/save", method = RequestMethod.GET)
    public Object twofactorloginSave(HttpServletRequest request, HttpServletResponse resp) throws IOException {

        try {
            Authentication acc = SecurityContextHolder.getContext().getAuthentication();
            logger.info(acc.getName());

            User user = new User();
            user.setUsername(acc.getName());
            user = userService.findByUsername(acc.getName());
            System.out.println("*****************" + user.getPassword());
            if ("true".equals(request.getParameter("enable"))) { 
                if (checkUserStatus(user)) {
                    user.setTwoFactorEnabled(true);
                } else {
                    return new ResponseEntity("not approved", HttpStatus.BAD_REQUEST);
                } 
            } else { 
                user.setTwoFactorEnabled(false);
            }
            userService.update(user);
            return new ResponseEntity("ok", HttpStatus.OK);
        } catch (Exception e) {
            logger.info("code not verified... redireted to logout page " + e.getMessage());
        }
        return new ResponseEntity("error", HttpStatus.SERVICE_UNAVAILABLE);
    }

    private boolean registerForTwoFactor(User user) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        try {
            UserVerifyModel userModel = new UserVerifyModel();
            userModel.setClientId(clientid);
            userModel.setClientSecret(clientSecret);
            userModel.setOrg_username(user.getUsername());
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            HttpEntity<UserVerifyModel> httpEntity = new HttpEntity(userModel, headers);
            logger.info("hitting the two factor register api.....");
            ResponseEntity<String> response = restTemplate.exchange(clientUrl + "/organization/registeruser", HttpMethod.PUT, httpEntity, String.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                String responseBody = response.getBody();
                System.out.println("************* " + responseBody);
                ObjectMapper mapper = new ObjectMapper();
                UserVerifyModel newUser = mapper.readValue(responseBody, UserVerifyModel.class);
                user.setVericationCode(newUser.getVerificationCode());
                System.out.println("************* " + user.getVericationCode());
                //user.setVerificationUrl(newUser.getVerificationUrl());
               // System.out.println("************* " +newUser.getVerificationUrl());
                user.setOrgid(newUser.getOrgId());
                return true;
            }
        } catch (RestClientException e) {
            logger.error("not able to register to two factor auth server ... please contant administrator.. " + e.getMessage());

        }
        return false;
    }

    private boolean checkUserStatus(User user) throws IOException {
        RestTemplate restTemplate = new RestTemplate();
        try {
            UserVerifyModel userModel = new UserVerifyModel();
            userModel.setClientId(clientid);
            userModel.setClientSecret(clientSecret);
            userModel.setOrg_username(user.getUsername());
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json");
            HttpEntity<UserVerifyModel> httpEntity = new HttpEntity(userModel, headers);
            logger.info("hitting the two factor status api.....");
            ResponseEntity<String> response = restTemplate.exchange(clientUrl + "/organization/users/status", HttpMethod.PUT, httpEntity, String.class);
            if (response.getStatusCode() == HttpStatus.OK) {
                String responseBody = response.getBody();
                return true;
            }
        } catch (RestClientException e) {
            logger.error("not able to register to two factor auth server ... please contant administrator.. " + e.getMessage());

        }
        return false;
    }

}
