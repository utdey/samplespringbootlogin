package com.hellokoding.auth.service;

import com.hellokoding.auth.model.User;

public interface UserService {
    void save(User user);
    void update(User user);

    User findByUsername(String username);
}
