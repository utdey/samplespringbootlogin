/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hellokoding.auth;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 *
 * @author subhajgh
 */
@Component
public class TwoFactorVerifyFilter implements Filter {

    @Override
    public void init(FilterConfig fc) throws ServletException {
        //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) sr;
        HttpServletResponse res = (HttpServletResponse) sr1;

        String requestPath = req.getRequestURI();
        
        
        List<String> urls = requriredUrls();
        if (urls.contains(requestPath)) {
            if (checkCookieExists(req, "http-secure-id", "pending")) {
                fc.doFilter(sr, sr1);
                return;
            } else {
                res.sendRedirect("/logout");
                return;
            }
        } else if (requestPath.equals("/welcome")  || requestPath.contains("twofactor")) {
            if (checkCookieExists(req, "http-secure-id", "verified")) {
                fc.doFilter(sr, sr1);
                return;
            } else {
                res.sendRedirect("/logout");
                return;
            }
        }
        fc.doFilter(sr, sr1);

    }

    private List<String> requriredUrls() {
        List<String> urls = new ArrayList<>();
        urls.add("/otplogin");
        urls.add("/twofactorlogin");
        return urls;
    }

    private boolean checkCookieExists(HttpServletRequest req, String cookieName, String val) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(cookieName) && cookie.getValue().equals(val)) {
                    return true;

                }
            }
        }
        return false;
    }

    @Override
    public void destroy() {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
