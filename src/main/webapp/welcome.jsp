<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Create an account</title>

        <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <div class="container">

            <c:if test="${pageContext.request.userPrincipal.name != null}">
                <form id="logoutForm" method="POST" action="${contextPath}/logout">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </form>

                <h2>Welcome ${pageContext.request.userPrincipal.name} | <a onclick="document.forms['logoutForm'].submit()">Logout</a></h2>

                <br><br>

                <p>  <input type="checkbox" id="twoFacEnable" value="${twoFactorEnabled}" onclick="onTwoFactorEnabled()" >Enable Two Factor </input></p>
                <div id="twofaccontainer">


                    <p id="instruction" style="display:none">Scan The QR Code With Your Authentocator App</p>


                    <p id="status">&nbsp;</p>
                    <div id="qrcode" style="margin-left: 10%"></div>
                    <p>&nbsp;</p>

                    <button id="twoFactorSave" onclick="onSaveTwoFactor()">Save</button>
                </div>


            </c:if>

        </div>


        <script>
            var elem = document.querySelector("#twoFacEnable");
            var qrScheduler = null;
            if (elem.value == "true") {
                elem.checked = true;
            } else {
                elem.checked = false;
            }
            function makeEnableCall() {
                $.ajax({
                    async: false,
                    type: "GET",
                    url: "/twofactor/enable",
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    },
                    datatype: 'json',
                    success: function (data) {
                        $("#twofaccontainer").show();
                        $("#instruction").show();
                        jQuery(function () {
                            document.querySelector("#qrcode").innerHTML = "";

                            jQuery('#qrcode').qrcode(JSON.stringify(data));
                        });
                    },
                    error: function (data) {
                        if (data.status == 200) {
                            $("#twofaccontainer").show();
                            document.querySelector("#verifyCode").innerHTML = "Verify Code :" + data.responseText;
                        }
                    }
                });
            }
            function onTwoFactorEnabled() {
                var elem = document.querySelector("#twoFacEnable");

                if (elem.checked === true) {
                    makeEnableCall();
                    if (!qrScheduler) {
                        qrScheduler = setInterval(function () {
                            makeEnableCall();
                        }, 30000);
                    }
                } else {
                    qrScheduler=clearInterval(qrScheduler);
                }
            }

            function onSaveTwoFactor() {
                var elem = document.querySelector("#twoFacEnable");

                $.ajax({
                    async: false,
                    type: "GET",
                    url: "/twofactor/save?enable=" + elem.checked,
                    headers: {
                        "Content-Type": "application/json"
                    },
                    success: function (data) {
                        if (elem.checked)
                            document.querySelector("#status").innerHTML = "User successfully enabled two factor through app";
                        else
                            document.querySelector("#status").innerHTML = "User disabled the two factor auth";
                        qrScheduler=clearInterval(qrScheduler);
                    },
                    error: function (data) {
                        document.querySelector("#status").innerHTML = "Two factor not enabled. Please verify the above code in Authenticator App";
                    }
                });

            }
        </script>
        <!-- /container -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="${contextPath}/resources/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.qrcode/1.0/jquery.qrcode.min.js"></script>

    </body>
</html>
